using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AttackerBehaviour : MonoBehaviour
{
    public ParticleSystem slashEffect;

    public GameObject attacker;
    public GameObject defender;
    public GameObject hitEffectGameObj;

    // Start is called before the first frame update
    void Start()
    {
        slashEffect.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowSlash()
    {
        slashEffect.Play();
    }

    private void OnMouseDown()
    {
        GetComponent<Animator>().SetBool("isAttack", true);
    }

    public void OnMakeAttack ()
    {
        defender.GetComponent<Animator>().SetBool("isHurt", true);

        GameObject hitObj = Instantiate(this.hitEffectGameObj);
        hitObj.transform.parent = defender.transform;
        hitObj.transform.position = defender.transform.position + new Vector3(0.0f, 1.0f, 0.0f);

        Sequence sequence = DOTween.Sequence();
        sequence.AppendInterval(1.0f)
            .AppendCallback(() =>
            {
                Destroy(hitObj);
            });
    }

    public void OnFinishAttackAnimation()
    {
        GetComponent<Animator>().SetBool("isAttack", false);
    }
}
